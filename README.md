[![pipeline](https://gitlab.com/naufalauddin/ppw-tk-2/badges/master/pipeline.svg)](https://tutorq-2.herokuapp.com)
![coverage](https://gitlab.com/naufalauddin/ppw-tk-2/badges/master/coverage.svg) 
# Tugas Kelompok PPW KD-14 Tutorku

### Anggota kelompok:
* Naufal ALauddin Hilmi
* Rony Agus Vian
* Wibias Fuad Al Khairi
* Willy Sandi Harsono

### Penjelasan Aplikasi
Kami akan membuat aplikasi data tutor yang berguna untuk tutor maupun tutee. Aplikasi ini akan membantu tutor dalam mengatur jadwal tutoring dan membroadcast
jadwalnya untuk memberikan kesempatan tutee untuk mengikutinya. Selain itu, tutor dapat memberikan spesifikasi pada jadwal tutorial untuk memberikan batasan
jumlah tutee yang diinginkan. Untuk tutee, aplikasi ini akan membantu tutee dalam mencari tutor dengan kuliah yang diinginkan dan mencari tutor dengan jadwal yang sesuai.

### Fitur-Fitur yang Diimplementasikan
Fitur yang akan diimplementasikan pada aplikasi kami adalah:
* Sign Up Tutor  
  Pada Signup Tutor, tutor dapat melakukan signup. Ketika melakukan signup, data yang perlu diisi untuk mendaftar adalah:  
  * Username
  * Password
  * Nama Lengkap
* Membuat Jadwal dan Detail Melihat Detail Jadwal  
  Ketika Tutor sudah melakukan sign up atau login, tutor akan dibawa ke laman yang berisikan list jadwal-jadwal yang pernah dibuatnya (untuk sign up list ini
  masih kosong diganti dengan tulisan "jadwalmu masih kosong, ayo buat jadwal!"), dan tombol untuk membuat jadwal. Ketika tutor memencet jadwal pada list,
  akan tutor akan dibawa ke laman yang berisikan detail dari jadwal dan tutee-tutee yang mengikuti sesi tersebut. Untuk membuat jadwal, tutor akan dibawa ke
  laman di mana tutor dapat membuat jadwal baru.
* Sign Up Tutee  
  Pada Sign Up Tutee, tutee dapat melakukan sign up. Untuk sign up tutee perlu megisi data berikut:  
  * Username
  * Password
  * Nama Lengkap
* Pencarian Jadwal  
  Tutee yang telah melakukan Login maupun Sign up akan dibawa ke laman yang berisikan list jadwal yang telah ia ikuti. Selain itu, tutee dapat pergi menuju
  laman pencarian jadwal. Pada laman tersebut, tutee dapat mecari jadwal berdasarkan mata kuliah.

### Link Heroku
<https://tutorq-2.herokuapp.com/>
