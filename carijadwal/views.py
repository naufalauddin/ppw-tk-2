from django.shortcuts import render, redirect
from .models import RelasiJadwal
from .forms import SearchForm
from django.views.generic import TemplateView, ListView
from django.db.models import Q
from buat_jadwal.models import Jadwal


def add(request, add_id):
        tutee_add = request.user;
        jadwal_add = Jadwal.objects.get(id=add_id)

        relasi = RelasiJadwal(tutee=tutee_add, jadwal=jadwal_add)
        relasi.save()
                
        #jadwal_form = JadwalForm(request.POST or None, initial=ubah, instance=jadwal_change)
        #jadwal_form.save()

        return render(request, 'carijadwal/home.html')
def delete(request, delete_id):
        pass

        #return render(request, 'home.html')

def lihatjadwal(request):
        model = Jadwal
        template_name = 'carijadwal/list.html'
        relasi_relasi = RelasiJadwal.objects.filter(tutee=request.user)
        object_list = []

        for relasi in relasi_relasi:
            r = Jadwal.objects.get(pk=relasi.jadwal.id)
            if(r not in object_list): object_list.append(r)
        
        context = {'object_list': object_list}
        return render(request, template_name, context)
        

# def home(request):
#     if request.method == 'POST':
#         form = ContactForm(request.POST)
#         if form.is_valid():
#             pass  # does nothing, just trigger the validation
#     else:
#         form = ContactForm()
#     return render(request, 'search.html', {'form': form})

class HomePageView(TemplateView):
    template_name = 'carijadwal/home.html'

class SearchResultsView(ListView):
    model = Jadwal
    template_name = 'carijadwal/search_results.html'

    def get_queryset(self):
        query1 = self.request.GET.get('q1')
        query2 = self.request.GET.get('q2')
        object_list = Jadwal.objects.filter(
            Q(matkul__icontains=query1) & Q(hari__icontains=query2)
        )
        return object_list
