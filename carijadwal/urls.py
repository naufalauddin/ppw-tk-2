from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'carijadwal'

urlpatterns = [
        # url(r'^add/(?P<add_id>[0-9]+)$', views.add, name='add'),
        # url(r'^delete/(?P<delete_id>[0-9]+)$', views.delete, name='delete'),
        # url(r'^$', HomePageView.as_view(), name='home'),
        # url(r'^search/$', SearchResultsView.as_view(), name='search_results'),
        path('add/<int:add_id>', views.add, name="add"),
        path('', views.HomePageView.as_view(), name="home"),
        path('lihatjadwal', views.lihatjadwal, name="lihatjadwal"),
        path('search/', views.SearchResultsView.as_view(), name="search_results"),

]
