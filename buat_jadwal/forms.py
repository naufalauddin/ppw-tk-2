from django import forms
from .models import Jadwal

class BuatJadwalForms(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        jam_mulai = cleaned_data.get('jam_mulai')
        jam_selesai = cleaned_data.get('jam_selesai')

        if jam_selesai and jam_mulai:
            if jam_selesai < jam_mulai:
                self.add_error(
                    'jam_selesai',
                    'Tolong untuk mengisi jam selesai di atas dari jam mulai'
                )

        return self.cleaned_data

    class Meta:
        model = Jadwal
        fields = ['hari', 'matkul', 'jam_mulai', 'jam_selesai']
        widgets = {
            'hari': forms.Select(
            ),
            'jam_mulai': forms.TimeInput(
                attrs={
                    'type':'time'
                }
            ),
            'jam_selesai': forms.TimeInput(
                attrs={
                    'type':'time'
                }
            ),
            'is_hidden':False
        }
    
