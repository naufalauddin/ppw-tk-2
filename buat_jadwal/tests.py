from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.shortcuts import reverse

from .forms import BuatJadwalForms
from .models import Jadwal

# Create your tests here.
class BuatJadwal(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = get_user_model().objects.create_user (
            username='nama_test',
            password='test'
        )
        cls.client = Client()
        
    def test_url_login(self):
        self.client.login(username='nama_test', password='test')
        
        response = self.client.get(reverse('buat_jadwal:buat'))
        self.assertEquals(response.status_code, 200)
    
    def test_url_not_login(self):
        response = self.client.get(reverse('buat_jadwal:buat'))
        self.assertEquals(response.status_code, 302)
    
    def test_post_jadwal_not_login(self):
        response = self.client.post(reverse('buat_jadwal:buat'), {
            "hari": "SENIN",
            "matkul": "Matdis",
            "jam_mulai": "10:00",
            "jam_selesai": "12:00"
        })

        self.assertEqual(response.status_code, 302)

        self.assertEqual(Jadwal.objects.all().count(), 0)
    
    def test_post_jadwal_login(self):
        self.client.login(username='nama_test', password='test')
        
        response = self.client.post(reverse('buat_jadwal:buat'), {
            "hari": "SENIN",
            "matkul": "Matdis",
            "jam_mulai": "10:00",
            "jam_selesai": "12:00"
        })

        jadwals = Jadwal.objects.all()
        self.assertEqual(jadwals.count(), 1)
        jadwal = jadwals[0]
        self.assertEqual(jadwal.tutor, self.user)
    
    def test_jadwal_time_backward(self):
        jadwal = {
            "hari": "SENIN",
            "matkul": "Matdis",
            "jam_mulai": "12:00",
            "jam_selesai": "10:00"
        }
        form = BuatJadwalForms(jadwal)

        self.assertEqual(form.is_valid(), False)
    
    def test_not_login_get_to_list_jadwal(self):
        response = self.client.get(reverse('buat_jadwal:list'))
        self.assertEqual(response.status_code, 302)
    
    def test_login_get_to_list_jadwal(self):
        self.client.login(username='nama_test', password='test')
        response = self.client.get(reverse('buat_jadwal:list'))
        self.assertEqual(response.status_code, 200)
        
    def test_login_list_jadwal(self):
        Jadwal.objects.create(
            hari="SENIN",
            matkul="Matdis",
            jam_mulai= "12:00",
            jam_selesai= "10:00",
            tutor=self.user
        )
        self.client.login(username='nama_test', password='test')
        response = self.client.get(reverse('buat_jadwal:list'))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'Matdis')