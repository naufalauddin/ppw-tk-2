from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .forms import BuatJadwalForms
from .models import Jadwal
from carijadwal.models import RelasiJadwal

# @login_required
# @tutor_required
def buat(request):
    if not request.user.is_authenticated:
        return redirect('accounts/login')

    if request.method == 'POST':
        form = BuatJadwalForms(request.POST)
        if form.is_valid():
            jadwal = form.save(commit=False)
            jadwal.tutor = request.user
            jadwal.save()
            return HttpResponseRedirect(reverse('buat_jadwal:list'))
    else:
        form = BuatJadwalForms()
    return render(
        request,
        'buat_jadwal/buat_jadwal.html',
        {
            'form': form,
        }
    )

def berhasil(request):
    return render(request, 'buat_jadwal/berhasil.html')

def list_jadwal(request):
    
    if not request.user.is_authenticated:
        return redirect('account/login')

    jadwals = Jadwal.objects.filter(tutor=request.user)

    for jadwal in jadwals:
        tutee2 = []
        relasi_relasi = RelasiJadwal.objects.filter(jadwal=jadwal)
        print(relasi_relasi)
        for relasi in relasi_relasi:
            tutee = relasi.tutee
            print(tutee)
            tutee2.append(tutee)
        jadwal.tutee = tutee2
    
    for jadwal in jadwals:
        print(jadwal.tutee)

    return render(request, 'buat_jadwal/list.html', {
        'jadwals': jadwals
    })

def delete(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            id_jadwal = request.POST.get('id')
            
            jadwal = Jadwal.objects.filter(pk=id_jadwal).first()

            if jadwal.tutor == request.user:
                jadwal.delete()
                return JsonResponse({"berhasil": True}, status=200)