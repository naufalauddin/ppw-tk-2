$(document).ready(function() {
    $('.accordion').click( function() {
        $(this).parent(".jadwal").siblings(".jadwal").children(".hideable").slideUp("slow");
        $(this).siblings('.hideable').slideToggle('slow');
    });
});

$(document).on('click', '.delete-jadwal', function(){
    btn = $(this);
    $.ajax({
        url: '/buatJadwal/delete/',
        type: 'POST',
        datatype: 'json',
        data: {
            csrfmiddlewaretoken: $(this).siblings('input[name=csrfmiddlewaretoken]').attr('value'),
            id: $(this).attr('value')
        },
        success: function() {
            console.log("me");
            id = btn.attr('value');
            console.log(btn);
            console.log(id);
            $(`div[val=${id}]`).remove();
        }
    })
})