from django.urls import path
from . import views

app_name = 'buat_jadwal'

urlpatterns = [
    path('', views.buat, name='buat'),
    path('berhasil', views.berhasil, name='berhasil'),
    path('list', views.list_jadwal, name='list'),
    path('delete/', views.delete, name='delete')
]
