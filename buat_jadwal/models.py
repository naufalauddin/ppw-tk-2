from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
class Jadwal(models.Model):
    '''
    This is a model for Jadwal
    '''
    HARI = [
        ('SENIN', 'Senin'),
        ('SELASA', 'Selasa'),
        ('RABU', 'Rabu'),
        ('KAMIS', 'Kamis'),
        ('JUMAT', 'Jumat'),
        ('SABTU', 'Sabtu'),
        ('MINGGU', 'Minggu')
    ]

    tutor = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    matkul = models.CharField(
        max_length=20
    )

    hari = models.CharField(
        max_length=6,
        choices=HARI,
    )

    jam_mulai = models.TimeField()

    jam_selesai = models.TimeField()
