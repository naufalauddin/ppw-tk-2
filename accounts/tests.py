from django.test import TestCase,Client
from django.urls import resolve
from .views import SignUpView
from django.apps import apps
from .apps import AccountsConfig

class AccountsPWWTK2Test(TestCase):
    def test_apps(self):
        self.assertEqual(AccountsConfig.name, 'tutor')
        self.assertEqual(apps.get_app_config('accounts').name, 'accounts')
    def test_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)#test existing url
        response = Client().get('/accounts/login')
        self.assertEqual(response.status_code, 301)
        response = Client().get('/accounts/signup')
        self.assertEqual(response.status_code, 301)
